package com.scholastic.coreservices.autoconfigure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Functions as a smoke test.
 */
@SpringBootApplication
public class SampleCoreServicesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SampleCoreServicesApplication.class, args);
    }

}