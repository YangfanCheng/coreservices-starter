package com.scholastic.coreservices.autoconfigure;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(prefix="coreservices", name="enabled", havingValue = "true") // coreservices.enabled=true
@EnableConfigurationProperties(CoreServicesProperties.class)
public class CoreServicesAutoConfiguration {

    @Bean
    public MyBean mybean() {
        return new MyBean("hello", "world");
    }

    public class MyBean {
        String foo;
        String bar;

        public MyBean(String foo, String bar) {
            this.foo = foo;
            this.bar = bar;
        }

        @Override
        public String toString() {
            return "MyBean{" +
                    "foo='" + foo + '\'' +
                    ", bar='" + bar + '\'' +
                    '}';
        }
    }
}
