package com.scholastic.coreservices.autoconfigure;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * Custom properties to enable each feature depending on application needs.
 * Note that such config set up is immutable, and a default {@code META-INF/spring-configuration-metadata.json} is generated at compile time.
 */
@ConfigurationProperties("coreservices")
@ConstructorBinding
@Getter
public class CoreServicesProperties {

    private final boolean enabled;
    @NestedConfigurationProperty
    private final Swagger swagger;
    @NestedConfigurationProperty
    private final Logging logging;

    public CoreServicesProperties(boolean enabled, Swagger swagger, Logging logging) {
        this.enabled = enabled;
        this.swagger = swagger;
        this.logging = logging;
    }

    @Getter
    public static class Swagger {
        private final boolean enabled;
        private final String projectName;
        private final String projectVersion;

        public Swagger(boolean enabled, String projectName, String projectVersion) {
            this.enabled = enabled;
            this.projectName = projectName;
            this.projectVersion = projectVersion;
        }
    }

    @Getter
    public static class Logging {
        private final boolean enabled;

        public Logging(boolean enabled) {
            this.enabled = enabled;
        }
    }

}
