package com.scholastic.coreservices.autoconfigure;

import com.google.common.collect.Maps;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Map;
import java.util.Optional;

/**
 * Auto-Configured Bean for AOP logging. Original implementation is done by {@author Joseph Strauss}. Further contribution appreciated.
 */
@Aspect
@Log4j2
@Configuration
@ConditionalOnClass({com.google.common.collect.Maps.class, Aspect.class})
@ConditionalOnProperty(
        value = "coreservices.logging.enabled",
        havingValue = "true",
        matchIfMissing = true
)
@ConditionalOnBean(CoreServicesAutoConfiguration.class)
@AutoConfigureAfter(CoreServicesAutoConfiguration.class)
public class CoreServicesLoggingAutoConfiguration {

    private static final Map<Class<?>, Logger> LOGGERS = Maps.newConcurrentMap();

    private Logger logger(JoinPoint joinPoint) {
        val clazz = joinPoint.getTarget().getClass();
        return LOGGERS.computeIfAbsent(clazz, LoggerFactory::getLogger);
    }

    @Pointcut("within(@org.springframework.web.bind.annotation.RestController *)")
    void restController() {
        // empty method
    }

    @Pointcut("within(@org.springframework.stereotype.Service *)")
    void serviceBeans() {
        // empty method
    }

    @Pointcut("execution(* *.*(..))")
    void allMethods() {
        // empty method
    }

    @Before("restController() && allMethods()")
    void beforeController(JoinPoint joinPoint) {
        val log = logger(joinPoint);
        val attributes = RequestContextHolder.currentRequestAttributes();
        if (attributes instanceof ServletRequestAttributes) {
            val request = ((ServletRequestAttributes) attributes).getRequest();
            val name = joinPoint.getSignature().getName();
            log.info("{} {}{}",
                    request.getMethod(),
                    request.getServletPath(),
                    Optional.ofNullable(request.getQueryString()).map(q -> "?" + q).orElse(""));
            log.debug("{}() - args:  {}", name, joinPoint.getArgs());
        }
    }

    @AfterThrowing(value = "serviceBeans() && allMethods()", throwing = "exception")
    void afterThrowing(JoinPoint joinPoint, Throwable exception) {

        val log = logger(joinPoint);
        log.error("{}({}) threw an error (exception={})",
                joinPoint.getSignature().getName(),
                joinPoint.getArgs(),
                exception.toString());
    }
}
