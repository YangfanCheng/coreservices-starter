package com.scholastic.coreservices.autoconfigure;

import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(CoreServicesAutoConfiguration.class)
public @interface CoreServices {

    /**
     * Specify what AutoConfiguration class to exclude
     */
    Class<?>[] exclude() default {};

}
