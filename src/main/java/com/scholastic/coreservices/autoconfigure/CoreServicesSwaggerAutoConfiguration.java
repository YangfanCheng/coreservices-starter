package com.scholastic.coreservices.autoconfigure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.boot.starter.autoconfigure.OpenApiAutoConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Optional;

import static springfox.documentation.builders.RequestHandlerSelectors.basePackage;

/**
 * Auto-Configured Bean for OpenAPI/Swagger.
 */
@Configuration
@ConditionalOnClass(OpenApiAutoConfiguration.class)
@ConditionalOnProperty(
        value = "coreservices.swagger.enabled",
        havingValue = "true",
        matchIfMissing = true
)
@ConditionalOnBean(CoreServicesAutoConfiguration.class)
@AutoConfigureAfter(CoreServicesAutoConfiguration.class)
public class CoreServicesSwaggerAutoConfiguration {

    @Autowired
    private CoreServicesProperties props;

    @Bean
    @ConditionalOnMissingBean(Docket.class)
    public Docket api() {
        String projectName = Optional.of(props)
                .map(CoreServicesProperties::getSwagger)
                .map(CoreServicesProperties.Swagger::getProjectName)
                .orElse("A CoreServices Project");

        String version = Optional.of(props)
                .map(CoreServicesProperties::getSwagger)
                .map(CoreServicesProperties.Swagger::getProjectVersion)
                .orElse("0.0.1");

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                        .title(projectName)
                        .version(version)
                        .build())
                .select()
                .apis(basePackage("com.scholastic"))
                .build();
    }
}
