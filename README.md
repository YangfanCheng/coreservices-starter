# CoreServices Starter

 A CoreServices project aims at abstracting away common dependencies and various configuration bean definitions.

# Getting started

Here is a quick maven set up:

```
    <repositories>
        <repository>
            <id>artifactory</id>
            <url>https://scholastic.jfrog.io/artifactory/Foundation</url>
        </repository>
    </repositories>

    ...
    <dependencies>
        ...
        <dependency>
            <groupId>com.scholastic.coreservices</groupId>
            <artifactId>coreservices-spring-boot-starter</artifactId>
            <version>0.0.1</version>
        </dependency>
        <!-- if intend to use Swagger -->
        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-boot-starter</artifactId>
            <version>3.0.0</version>
        </dependency>
        ...
    </dependencies>
```

Note that if transitive dependencies isn't enabled, you must specify each dependency in the pom.xml, as some Auto Configured Beans depend on library such as SpringFox, an example is as follows:
```
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-boot-starter</artifactId>
    <version>3.0.0</version>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-aop</artifactId>
</dependency>
<dependency>
    <groupId>com.google.guava</groupId>
    <artifactId>guava</artifactId>
    <version>29.0-jre</version>
</dependency>
```

Once downloaded, various Auto Configuration beans can be activated/deactivated in following examples:
```
# application.properties
coreservices.enabled=true
coreservices.swagger.enabled=true
coreservices.swagger.project-name="yop"
coreservices.swagger.project-version="0.0.2"

# application.yml
coreservices:
  enabled: true
  swagger:
    project-name: "coreservices-hello-world"
    project-version: "0.0.2"
  logging:
    enabled: false
```

# Contributor Notes

As this CoreServices project is still in its infant state, feel free to add missing commonly used features such as Spring Cloud Consul. 